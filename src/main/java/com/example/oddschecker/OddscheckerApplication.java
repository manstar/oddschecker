package com.example.oddschecker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OddscheckerApplication {

	public static void main(String[] args) {
		SpringApplication.run(OddscheckerApplication.class, args);
	}

}
