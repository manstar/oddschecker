package com.example.oddschecker.controllers;

import com.example.oddschecker.dtos.OddsDto;
import com.example.oddschecker.services.OddsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/odds")
public class OddsController {

        @Autowired
        OddsService oddsService;

        @GetMapping("/{betId}")
        public ResponseEntity<List<OddsDto>> getOddsByBetId(@PathVariable Long betId) {
               return new ResponseEntity<>(oddsService.getOddsByBetId(betId), HttpStatus.OK);
        }

        @PostMapping
        public ResponseEntity<Object> addOdds(@Valid @RequestBody OddsDto oddDto) {
                final Long savedMovieDto = oddsService.createOdds(oddDto);
                if(savedMovieDto == null){
                        return new ResponseEntity<>(HttpStatus.NOT_ACCEPTABLE);
                }
                return new ResponseEntity<>("Odds have been created for bet", HttpStatus.CREATED);
        }

}
