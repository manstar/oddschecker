package com.example.oddschecker.utils;

import com.example.oddschecker.dtos.OddsDto;
import com.example.oddschecker.entities.Bet;
import com.example.oddschecker.entities.Odds;

public final class OddsToolkit {

    public static Bet transformOddDtoToBet(OddsDto oddsDto){
            final Bet bet = new Bet();
            bet.setBetId(oddsDto.getBetId());
            return bet;
    }

    public static OddsDto transformOddsToOddsDto(Odds odds, Long betId){
        final OddsDto oddsDto = new OddsDto();
        oddsDto.setBetId(betId);
        oddsDto.setUserId(odds.getUser().getUserId());
        oddsDto.setOdds(odds.getOdds());
        return oddsDto;
    }
}
