package com.example.oddschecker.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class OddsKey implements Serializable {


    @Column(name = "user_id")
    private String userId;

    @Column(name = "bet_id")
    private Long betId;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((userId == null) ? 0 : userId.hashCode());
        result = prime * result
                + ((betId == null) ? 0 : betId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        OddsKey other = (OddsKey) obj;
        return Objects.equals(getUserId(), other.getUserId()) && Objects.equals(getBetId(), other.getBetId());
    }
}