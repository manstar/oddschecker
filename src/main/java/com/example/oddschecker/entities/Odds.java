package com.example.oddschecker.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Odds {

    @EmbeddedId
    private OddsKey id = new OddsKey();

    @Column(name = "odds")
    private String odds;

    @ManyToOne
    @MapsId("betId")
    @JoinColumn(name="bet_id")
    private Bet bet;

    @ManyToOne
    @MapsId("userId")
    @JoinColumn(name="user_id")
    private User user;

}
