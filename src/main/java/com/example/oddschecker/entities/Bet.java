package com.example.oddschecker.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="bets")
public class Bet {

        @Id
        @Column(name = "bet_id", nullable = false)
        private Long betId;

        @Column(name = "description")
        private String description;

        @OneToMany(mappedBy = "bet")
        private List<Odds> odds = new ArrayList<>();

}
