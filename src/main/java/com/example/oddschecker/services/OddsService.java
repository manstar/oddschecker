package com.example.oddschecker.services;

import com.example.oddschecker.dtos.OddsDto;
import com.example.oddschecker.entities.Odds;

import java.util.List;

public interface OddsService {

    Long createOdds(OddsDto oddDto);

    List<OddsDto> getOddsByBetId(Long betId);

    List<Odds> getAllOdds();

}
