package com.example.oddschecker.services;

import com.example.oddschecker.daos.BetsDAO;
import com.example.oddschecker.dtos.OddsDto;
import com.example.oddschecker.entities.*;
import com.example.oddschecker.entities.Odds;
import com.example.oddschecker.repos.OddsRepository;
import com.example.oddschecker.repos.UsersRepository;
import com.example.oddschecker.utils.OddsToolkit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

import static java.util.stream.Collectors.toList;

@Service
public class OddsServiceImpl implements OddsService {

    @Autowired
    BetsDAO betsDAO;

    @Autowired
    UsersRepository usersRepository;

    @Autowired
    OddsRepository oddsRepository;

    @Override
    public Long createOdds(OddsDto oddsDto) {
        final Bet newBet = new Bet();
        newBet.setBetId(oddsDto.getBetId());
        betsDAO.saveBet(newBet);

        final User newUser = new User();
        newUser.setUserId(oddsDto.getUserId());
        usersRepository.save(newUser);

        final OddsKey key = new OddsKey(oddsDto.getUserId(), oddsDto.getBetId());
        if(oddsRepository.existsById(key)){
            oddsRepository.updateOdds(oddsDto.getUserId(), oddsDto.getBetId(), oddsDto.getOdds());

        } else {
            oddsRepository.insertOdds(oddsDto.getBetId(),oddsDto.getUserId(), oddsDto.getOdds());
        }
        return oddsDto.getBetId();
    }

    @Override
    public List<OddsDto> getOddsByBetId(Long betId) {
        return betsDAO.getOddsByBetId(betId)
                .getOdds()
                .stream()
                .filter(Objects::nonNull)
                .map(x->OddsToolkit.transformOddsToOddsDto(x,betId))
                .collect(toList());
    }

    @Override
    public List<Odds> getAllOdds() {
        return oddsRepository.findAll();
    }
}
