package com.example.oddschecker.daos;

import com.example.oddschecker.entities.Bet;
import com.example.oddschecker.repos.BetsRepository;
import com.example.oddschecker.repos.OddsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class BetsDAO {

    @Autowired
    private BetsRepository betsRepo;

    @Autowired
    private OddsRepository oddsRepo;


    public Bet saveBet(Bet bet) {
        return betsRepo.save(bet);
    }

    public Bet getOddsByBetId(Long betId) {
        return betsRepo.getById(betId);
    }

}
