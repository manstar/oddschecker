package com.example.oddschecker.repos;

import com.example.oddschecker.entities.Bet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BetsRepository extends JpaRepository<Bet, Long> {



}
