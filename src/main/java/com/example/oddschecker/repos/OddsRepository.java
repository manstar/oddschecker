package com.example.oddschecker.repos;

import com.example.oddschecker.entities.Odds;
import com.example.oddschecker.entities.OddsKey;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;


@Repository
public interface OddsRepository extends JpaRepository<Odds, OddsKey> {

     @Transactional
     @Modifying
     @Query("Update Odds set odds = :odds where user_id = :userId and bet_id = :betId")
     Integer updateOdds(String userId,Long betId,String odds);

     @Transactional
     @Modifying
     @Query(value = "insert into Odds (bet_id,user_id,odds) values (:betId, :userId, :odds)" , nativeQuery = true)
     Integer insertOdds(Long betId,String userId,String odds);

}
