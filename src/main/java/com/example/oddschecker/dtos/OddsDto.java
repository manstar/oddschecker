package com.example.oddschecker.dtos;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

@Data
@NoArgsConstructor
public final class OddsDto {

    @NotBlank(message = "Invalid format of Odds")
    @Pattern(regexp = "[1-9][0-9]*/[1-9][0-9]*|SP", message = "Invalid format of Odds")
    private String odds;
    private Long betId;
    private String userId;
}
