package com.example.oddschecker.rest;

import com.example.oddschecker.TestToolkit;
import com.example.oddschecker.controllers.OddsController;
import com.example.oddschecker.dtos.OddsDto;
import com.example.oddschecker.repos.BetsRepository;
import com.example.oddschecker.services.OddsService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@WebMvcTest(OddsController.class)
public class OddsControllerTest {

	@Autowired
	protected MockMvc mockMvc;

	@MockBean
	protected OddsService oddsService;

	@MockBean
	protected BetsRepository betsRepository;

	@Autowired
	private ObjectMapper objectMapper;

	@Test
	public void shouldBeAddedOddsByUser() throws Exception {
		// given
		final OddsDto oddsDto = new OddsDto();
		oddsDto.setBetId(1L);
		oddsDto.setUserId(String.valueOf(1));
		oddsDto.setOdds("1/2");
		String body = objectMapper.writeValueAsString(oddsDto);
		// when
		when(oddsService.createOdds(oddsDto)).thenReturn(1L);
		// then
		mockMvc.perform(post("/odds")
				.content(body)
				.contentType(APPLICATION_JSON_VALUE)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isCreated())
				.andExpect(content().string("Odds have been created for bet"));

	}


	@Test
	public void shouldReturnOddsOfBet() throws Exception {
		mockMvc.perform(get("/odds/1").accept(APPLICATION_JSON_VALUE))
				.andExpect(content().contentType(APPLICATION_JSON_VALUE))
				.andExpect(status().isOk()).andDo(print());
	}

	@Test
	public void shouldNotAddedInvalidOdds() throws Exception {
		List<OddsDto> invalidOdds = TestToolkit.createInvalidOdds();
		for (OddsDto oddDto : invalidOdds){
			String body = objectMapper.writeValueAsString(oddDto);
			mockMvc.perform(post("/odds")
					.content(body)
					.contentType(APPLICATION_JSON_VALUE)
					.accept(MediaType.APPLICATION_JSON))
					.andExpect(status().isBadRequest())
					.andExpect(content().string("Invalid format of Odds"));
		}
	}

	@Test
	public void shouldNotAcceptInvalidBetId() throws Exception {
		mockMvc.perform(get("/odds/LL").accept(APPLICATION_JSON_VALUE))
				.andExpect(status().isBadRequest())
				.andExpect(content().string("Invalid Bet ID supplied"));
	}

}
