package com.example.oddschecker.service;


import com.example.oddschecker.dtos.OddsDto;
import com.example.oddschecker.entities.Odds;
import com.example.oddschecker.services.OddsService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

import java.util.ArrayList;
import java.util.List;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class OddsServiceTest {

    @Autowired
    protected OddsService oddsService;

    @Test
    public void shouldAddOddsOfBet() {
        List<OddsDto> odds = new ArrayList<>();
        final OddsDto oddDto1 = new OddsDto();
        oddDto1.setBetId(1L);
        oddDto1.setUserId(String.valueOf(1));
        oddDto1.setOdds("1/2");

        final OddsDto oddDto2 = new OddsDto();
        oddDto2.setBetId(1L);
        oddDto2.setUserId(String.valueOf(2));
        oddDto2.setOdds("3/2");

        final OddsDto oddDto3 = new OddsDto();
        oddDto3.setBetId(1L);
        oddDto3.setUserId(String.valueOf(3));
        oddDto3.setOdds("SP");
        odds.add(oddDto1);
        odds.add(oddDto2);
        odds.add(oddDto3);

        oddsService.createOdds(oddDto1);
        oddsService.createOdds(oddDto2);
        oddsService.createOdds(oddDto3);

        List<Odds> allOdds = oddsService.getAllOdds();

        assertThat("There should be 3 odds", allOdds, hasSize(3));
        assertThat("There is not odd with 1/2.", allOdds.stream().anyMatch(x->x.getOdds().equals("1/2")));
        assertThat("There is not odd with 3/2.", allOdds.stream().anyMatch(x->x.getOdds().equals("3/2")));
        assertThat("There is not odd with SP.", allOdds.stream().anyMatch(x->x.getOdds().equals("SP")));
    }

    @Test
    @Transactional
    public void shouldReturnedOddsByBetId() {
        long betId = 1;
        List<OddsDto> returnedOdds = oddsService.getOddsByBetId(betId);
        assertThat("There should be 1 odds", returnedOdds, hasSize(3));
        assertThat("There is not odd with 1/2.", returnedOdds.stream().anyMatch(x->x.getOdds().equals("1/2")));
        assertThat("There is not odd with 3/2.", returnedOdds.stream().anyMatch(x->x.getOdds().equals("3/2")));
        assertThat("There is not odd with SP.", returnedOdds.stream().anyMatch(x->x.getOdds().equals("SP")));
    }

}
