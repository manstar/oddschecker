package com.example.oddschecker;

import com.example.oddschecker.dtos.OddsDto;

import java.util.ArrayList;
import java.util.List;

public final class TestToolkit {

    public static List<OddsDto> createInvalidOdds(){
        List<OddsDto> invalidOdds = new ArrayList<>();
        final OddsDto oddDto1 = new OddsDto();
        oddDto1.setBetId(1L);
        oddDto1.setUserId(String.valueOf(1));
        oddDto1.setOdds("23*4");

        final OddsDto oddDto2 = new OddsDto();
        oddDto2.setBetId(1L);
        oddDto2.setUserId(String.valueOf(1));
        oddDto2.setOdds("1/2/3");

        final OddsDto oddDto3 = new OddsDto();
        oddDto3.setBetId(1L);
        oddDto3.setUserId(String.valueOf(1));
        oddDto3.setOdds("0/1");

        final OddsDto oddDto4 = new OddsDto();
        oddDto4.setBetId(1L);
        oddDto4.setUserId(String.valueOf(1));
        oddDto4.setOdds("10");

        final OddsDto oddDto5 = new OddsDto();
        oddDto5.setBetId(1L);
        oddDto5.setUserId(String.valueOf(1));
        oddDto5.setOdds("SP/1");

        final OddsDto oddDto6 = new OddsDto();
        oddDto6.setBetId(1L);
        oddDto6.setUserId(String.valueOf(1));

        invalidOdds.add(oddDto1);
        invalidOdds.add(oddDto2);
        invalidOdds.add(oddDto3);
        invalidOdds.add(oddDto4);
        invalidOdds.add(oddDto5);
        invalidOdds.add(oddDto6);

        return invalidOdds;
    }

}
